using System;
using UnityEngine;

public class RoadGenerator : MonoBehaviour
{
    [SerializeField] private GameObject[] roads;
    [SerializeField] private Transform[] roadsCenter;
    [SerializeField] private Transform[] roadsForestLeft;
    [SerializeField] private Transform[] roadsForestRight;
    [SerializeField] private float resetDistance  = 100000;
    [SerializeField] private float roadLength = 200;
    [SerializeField] private int currentRoad = 0;
    [SerializeField] private int roadsPassed = 1;
    [SerializeField] private Transform player;

    private void Start()
    {
        player = Player.St.GetComponent<Transform>();
        
        if (!PlayerPrefs.HasKey("LINES") || PlayerPrefs.GetInt("LINES") == 3 || GameManager.St.sceneName == "Menu")
            return;
        
        roadsCenter = roadsForestLeft = roadsForestRight = new Transform[roads.Length];
        for (var i = 0; i < roads.Length; i++)
        {
            roadsCenter[i] = roads[i].transform.GetChild(1);
            roadsCenter[i].localScale = new Vector3(roadsCenter[i].localScale.x / 3 * 5,roadsCenter[i].localScale.y,roadsCenter[i].localScale.z);
        }
        
        for (var i = 0; i < roads.Length; i++)
        { 
            roadsForestLeft[i] = roads[i].transform.GetChild(0);
            roadsForestLeft[i].position = new Vector3(roadsForestLeft[i].position.x - GameManager.St.distanceBetweenlines+1, roadsCenter[i].position.y,roadsCenter[i].position.z);
        }
        
        for (var i = 0; i < roads.Length; i++)
        { 
            roadsForestRight[i] = roads[i].transform.GetChild(2);
            roadsForestRight[i].position = new Vector3(roadsForestRight[i].position.x + GameManager.St.distanceBetweenlines-1, roadsCenter[i].position.y,roadsCenter[i].position.z);
        }
        
        
    }

    private void FixedUpdate()
    {
        if (player.position.z < roadLength * roadsPassed)
            return;
        
        roads[currentRoad].transform.Translate(0,0, roadLength * roads.Length);
        //roads[currentRoad].transform.position = new Vector3(0,0, roadLength * roads.Length * roadsPassed);
        roadsPassed++;
                
        if(currentRoad + 1 ==  roads.Length)
            currentRoad = 0;
        else currentRoad++;
        
        if (player.position.z >= resetDistance)
            RoadReset();
    }

    private void RoadReset()
    {
        roadsPassed = 1;
        
        Player.St.ResetPlayer(player.position.z - resetDistance);
        
        foreach (var obj in roads)
        {
            var position = obj.transform.position;
            obj.transform.position = new Vector3(position.x, position.y, position.z - resetDistance);
        }
        
        if(GameManager.St.sceneName != "Game")
            return;

        UnitsGenerator.St.last = 0;
        
        foreach (var obj in UnitsGenerator.St.objOnRoad)
        { 
            var position = obj.Value.transform.position; 
            obj.Value.transform.position = new Vector3(position.x, position.y, position.z - resetDistance);
        }
    }
}