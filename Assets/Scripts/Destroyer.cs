using UnityEngine;

public class Destroyer : MonoBehaviour
{ // Удаление падающих объектов
    [SerializeField] private float yLimit = 5.0f; // Предел падения по оси Y

    private void Start()
    {
        UnitsGenerator.St.objOnRoad.Add(gameObject,this);
        yLimit = 5.0f;
    }

    private void FixedUpdate()
    {
        if (transform.position.y < -yLimit)
        {
            UnitsGenerator.St.objOnRoad.Remove(gameObject);
            Destroy(gameObject);
        }
            
    }
}
