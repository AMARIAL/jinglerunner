using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sizer : MonoBehaviour
{// Меняет размеры объекта
    
    [SerializeField] private float min = 0.5f;
    [SerializeField] private float max = 1.5f;
    private void Start()
    {
        transform.localScale *= Random.Range(min,max);
    }
    
}
