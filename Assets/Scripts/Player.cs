using UnityEngine;

public class Player : MonoBehaviour
{// Сани, за которые играет игрок
    public static Player St  {get; private set;} // Player.St (Singltone)
    
    private CharacterController _character;
    [SerializeField] public float speed = 30f;
    [SerializeField] private float changeLineSpeed = 25f;

    [SerializeField] private float jumpPosition;
    [SerializeField] private float jumpForce = 10;
    [SerializeField] private float gravity = 20;
    
    [HideInInspector] public ParticleSystem sliders;

    private void Awake()
    {
        St = this;
    }

    private void Start()
    {
        _character = GetComponent<CharacterController>();
        sliders = GameObject.Find("FX Sliders").GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (!GameManager.St.gameOver)
        {
            Controls();
            Move();
        }
    }

    private void Move()
    {
        var targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;

        if (GameManager.St.curentLine == -2)
        {
            targetPosition += Vector3.left * (GameManager.St.distanceBetweenlines * 2); 
        } 
        else if (GameManager.St.curentLine == -1)
        {
            targetPosition += Vector3.left * GameManager.St.distanceBetweenlines;
        } 
        else if (GameManager.St.curentLine == 1)
        {
            targetPosition += Vector3.right * GameManager.St.distanceBetweenlines;
        }
        else if (GameManager.St.curentLine == 2)
        {
            targetPosition += Vector3.right * (GameManager.St.distanceBetweenlines * 2);
        }

        if (transform.position != targetPosition)
        {
            var diff = targetPosition - transform.position;
            var moveDir = diff.normalized * (changeLineSpeed * Time.deltaTime);
            if (moveDir.sqrMagnitude < diff.sqrMagnitude)
                _character.Move(moveDir);
            else
                _character.Move(diff);
        }
        
        if(sliders.isStopped && _character.isGrounded)
            sliders.Play();
    }

    private void Controls()
    {
        if (Swipe.St.swipeRight)
        {
            if (GameManager.St.lineCount == 3 && GameManager.St.curentLine < 1 || GameManager.St.lineCount == 5 && GameManager.St.curentLine < 2)
            {
                GameManager.St.curentLine++;
                Sound.St.PlaySound(Sounds.slide);
            }
            
        }
        
        if (Swipe.St.swipeLeft)
        {
            if (GameManager.St.lineCount == 3 && GameManager.St.curentLine > -1 || GameManager.St.lineCount == 5 && GameManager.St.curentLine > -2)
            {
                GameManager.St.curentLine--;
                Sound.St.PlaySound(Sounds.slide);
            }
        }
        
        if (Swipe.St.swipeUp)
        {
            if (_character.isGrounded)
                Jump();
        }
    }
    
    private void Jump()
    {
        jumpPosition = jumpForce;
        Sound.St.PlaySound(Sounds.jump);
        sliders.Stop();
    }
    
    public void ResetPlayer(float resetDistance)
    {
        _character.enabled = false;
        var position = transform.position;
        transform.position = new Vector3(position.x,position.y,resetDistance);
        GameManager.St.points += (int) position.z / 50;
        _character.enabled = true;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (!hit.gameObject.CompareTag("Enemy")) 
            return;
        GameManager.St.GameOver();
        Sound.St.PlaySound(Sounds.hit);
    }

    private void FixedUpdate()
    {
        if (GameManager.St.gameOver && GameManager.St.sceneName == "Game")
            return;
        
        if (!_character.isGrounded)
            jumpPosition -= gravity * Time.fixedDeltaTime;
            
        _character.Move(new Vector3(0,jumpPosition,speed)  * Time.fixedDeltaTime);
    }
}
