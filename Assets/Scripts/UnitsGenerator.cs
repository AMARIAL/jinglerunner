using System.Collections.Generic;
using UnityEngine;

public class UnitsGenerator : MonoBehaviour
{// генератор припятствий
    
    public static UnitsGenerator St  {get; private set;} // UnitsGenerator.St (Singltone)
    
    private Transform player;
    private float  playerYposition;
    private float[]  positionsXvariants;
    [SerializeField] private float startGeneratePosition = 200; // стартовая позиция генерации
    [SerializeField] private float range = 900; // дальность места генерации
    [SerializeField] private float periodic; // переодичность генерации
    [SerializeField] private float periodicMax = 100.1f; // максимальная переодичность генерации
    [SerializeField] public float last; // Последняя точка генерации
    [SerializeField] private GameObject[] enemies;
    [SerializeField] private GameObject[] items;

    public Dictionary<GameObject, Destroyer> objOnRoad;
    
    private void Awake()
    {
        St = this;
        objOnRoad = new Dictionary<GameObject, Destroyer>();
    }
    
    void Start()
    {
        player = Player.St.GetComponent<Transform>();
        var position = player.position;
        playerYposition = position.y + 0.5f;
        last = position.z;
        positionsXvariants = new float[5]
        {
            position.x-GameManager.St.distanceBetweenlines * 2,
            position.x-GameManager.St.distanceBetweenlines,
            position.x,
            position.x+GameManager.St.distanceBetweenlines,
            position.x+GameManager.St.distanceBetweenlines * 2
        };
        
        while (startGeneratePosition < range - periodicMax/4)
        {
            Generate(true);
            startGeneratePosition += Random.Range(periodicMax / 2, periodicMax);
        }
        
    }

 
    private void FixedUpdate()
    {
        if (player.position.z - last < periodic)
            return;
                
        Generate();
        
        last = player.position.z;
        periodic = Random.Range(periodicMax / 2, periodicMax);
    }

    private void Generate(bool isFirst = false)
    {
        var i = GameManager.St.lineCount == 5 ? Random.Range(0, 5) : Random.Range(1, 4);
        var z = isFirst ? startGeneratePosition : player.position.z + range;
        var pos = new Vector3(positionsXvariants[i], playerYposition, z);
            
        if (Random.Range(0,2) == 1)
        { 
            var rand = Random.Range(0,enemies.Length);
            Instantiate(enemies[rand], pos, Quaternion.Euler(0, Random.Range(135,225), 0), transform);
        }
        else
        {
            var rand = Random.Range(0,items.Length);
            Instantiate(items[rand], pos, Quaternion.Euler(0, Random.Range(135,225), 0), transform);
        }
        
    }
}
