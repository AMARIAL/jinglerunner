using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snow : MonoBehaviour
{// Преследование игрока снегом по оси Z
    [HideInInspector] public Transform player;
    
    private void Start()
    {
        player = Player.St.GetComponent<Transform>();
    }
    
    private void Update()
    {
        var position = transform.position;
        transform.position = new Vector3(position.x,position.y, player.position.z);
    }
}
