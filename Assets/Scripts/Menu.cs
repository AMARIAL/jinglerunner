using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public static Menu St  {get; private set;} // Menu.St (Singltone)

    private void Awake()
    {
        St = this;
    }

    public void ChangeThreeFiveLinesButtonText()
    {
        GameManager.St.linesButtonText.text = GameManager.St.lineCount == 3 ? "Three Lines Game" : "Five Lines Game";
    }
    
    public void StartGame()
    {
        SceneManager.LoadScene("Scenes/Game");
    }
    
    public void ThreeFiveLines()
    {
        if (PlayerPrefs.HasKey("LINES") && PlayerPrefs.GetInt("LINES") == 5)
            GameManager.St.lineCount = 3;
        else
            GameManager.St.lineCount = 5;
        
        PlayerPrefs.SetInt("LINES", GameManager.St.lineCount);
        PlayerPrefs.Save();

        ChangeThreeFiveLinesButtonText();
    }
    
    public void SoundOnOff()
    {
        Sound.St.SoundOnOff();
    }
    
    public void MusicOnOff()
    {
        Sound.St.MusicOnOff();
    }
    
    public void TryAgain()
    {
        Time.timeScale = 1;
        GameManager.St.gameOver = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
    }
    
    public void PauseGame(bool flag = true)
    {
        if (flag)
        {
            GameManager.St.pauseRecordText.text = "Record: " + GameManager.St.record;
            Time.timeScale = 0;
        }
        else
            Time.timeScale = 1;
    }
    
    public void QuitToMainMenu()
    {
        Time.timeScale = 1;
        GameManager.St.gameOver = true;
        SceneManager.LoadScene("Scenes/Menu");
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
    
}
