using UnityEngine;
public enum Sounds
{
    slide,
    jump,
    hit,
    take
}
public enum Music
{
    menu,
    game
}
public class Sound : MonoBehaviour
{
    public static Sound St  {get; private set;} // Sound.St (Singltone)
    
    public bool MusicOn = true;
    public bool SoundOn = true;
    
    private AudioSource musicAudio;
    
    [SerializeField] private AudioSource slide;
    [SerializeField] private AudioSource jump;
    [SerializeField] private AudioSource hit;
    [SerializeField] private AudioSource take;
    
    [SerializeField] private AudioClip menu;
    [SerializeField] private AudioClip game;
    
    private void Awake()
    {
        St = this;
        musicAudio = GetComponent<AudioSource>();
    }
    private void Start()
    {
        if (PlayerPrefs.HasKey("MUSIC") && PlayerPrefs.GetString("MUSIC") == "OFF")
            MusicOn = false;
        if (PlayerPrefs.HasKey("SOUND") && PlayerPrefs.GetString("SOUND") == "OFF")
            SoundOn = false;
        
        if (!MusicOn)
        {
            musicAudio.Stop();
        }
    }
    
    public void SoundOnOff()
    {
        SoundOn = !SoundOn;
        PlayerPrefs.SetString("SOUND", SoundOn ? "ON" : "OFF");
        PlayerPrefs.Save();
    }
    
    public void MusicOnOff()
    {
        MusicOn = !MusicOn;
        PlayerPrefs.SetString("MUSIC", MusicOn ? "ON" : "OFF");
        PlayerPrefs.Save();
        
        if (MusicOn)
        {
            musicAudio.Play();
        }
        else
        {
            musicAudio.Stop();
        }
    }
    public void PlayMusic(Music music)
    {
        switch (music)
        {
            case Music.menu: musicAudio.clip = menu; break;
            case Music.game: musicAudio.clip = game; break;
        }
        if(MusicOn)
            musicAudio.Play();
    }
    public void PlaySound(Sounds sound)
    {
        if (SoundOn)
        {
            switch (sound)
            {
                case Sounds.slide: slide.Play(); break;
                case Sounds.jump: jump.Play(); break;
                case Sounds.hit: hit.Play(); break;
                case Sounds.take: take.Play(); break;
            }
        }
    }
}
