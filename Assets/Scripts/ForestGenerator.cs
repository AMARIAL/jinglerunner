using UnityEngine;
using Random = UnityEngine.Random;

public class ForestGenerator : MonoBehaviour
{ // Генератор леса
    [SerializeField] private GameObject[] objects; // Объекты для генерации леса. Камни и деревья
    private int count = 100; // Количество объектов для генерации
    
    private void Start()
    {
        var j = 99; // погрешность места генерации. Чтобы деревья краями не лезли на трассу
        
        if (GameManager.St.lineCount == 5)
            j = 98;
        
        foreach (GameObject obj in objects)
        {
            for (int i = 0; i <= count; i++)
            {
                Vector3 position = transform.position;
                position = new Vector3(Random.Range(position.x - j, position.x + j), 0.0f, Random.Range(position.z-100, position.z+100));
                Instantiate(obj, position, Quaternion.Euler(0, Random.Range(0,360), 0), transform);
            }
        }
    }
    
}
