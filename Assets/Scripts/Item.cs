using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{// Подбор предметов

    [SerializeField] private int points = 10; // Количество очков за предмет

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            UnitsGenerator.St.objOnRoad.Remove(gameObject);
            Destroy(gameObject);
            GameManager.St.points += points;
            Sound.St.PlaySound(Sounds.take);
        }
    }
}
