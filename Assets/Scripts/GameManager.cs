using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class  GameManager : MonoBehaviour
{
    public static GameManager St  {get; private set;} // GameManager.St (Singltone)

    internal GameObject gameOverPanel;
    internal Text scoreTitle;
    internal Text linesButtonText;
    internal Text pauseRecordText;
    internal string currentScoreTitle;
    private Transform playerTransform;
    private Text scoreText;
    
    [SerializeField] public string sceneName;
    [SerializeField] public bool gameOver = true;
    [SerializeField] public int score;
    [SerializeField] public int points;
    [SerializeField] internal int record;
    [SerializeField] public int curentLine; // [-1 / 1] или [-2 / 2] текущая дорожка (0 это средняя)
    [SerializeField] public int lineCount = 3; // [3 или 5] количество дорожек
    [SerializeField] private int speedIncreaseSeconds = 1; // ускорение каждую секунду
    [SerializeField] public float distanceBetweenlines = 4; // растояние между центрами линий

    private void Awake()
    {
        St = this;
        record = PlayerPrefs.HasKey("RECORD") ? PlayerPrefs.GetInt("RECORD") : DateTime.Now.Year + 1;
        lineCount = PlayerPrefs.HasKey("LINES") ? PlayerPrefs.GetInt("LINES") : 3;
        sceneName = SceneManager.GetActiveScene().name;
    }

    private void Start()
    {
        score = 0;
        curentLine = 0;
        
        if (sceneName == "Game")
        {
            gameOver = false;
            
            scoreTitle = GameObject.Find("scoreTitle").GetComponent<Text>();
            currentScoreTitle = scoreTitle.text;
            pauseRecordText = GameObject.Find("recordText").GetComponent<Text>();
            scoreText = GameObject.Find("scoreText").GetComponent<Text>();
            
            GameObject.Find("Pause").SetActive(false);
            gameOverPanel = GameObject.Find("GameOver");
            gameOverPanel.SetActive(false);
            
            StartCoroutine(Coroutine("SpeedIncrease"));
            Sound.St.PlayMusic(Music.game);
        }
        else if (sceneName == "Menu")
        {
            gameOver = true;
            scoreText = GameObject.Find("recordText").GetComponent<Text>();
            scoreText.text = record.ToString();
            linesButtonText = GameObject.Find("linesButtonText").GetComponent<Text>();
            Sound.St.PlayMusic(Music.menu);
            Menu.St.ChangeThreeFiveLinesButtonText();
        }
        
        playerTransform = Player.St.GetComponent<Transform>();
        
    }
    
    private void Update()
    {
        if (!gameOver)
           scoreText.text = score.ToString();
    }
    
    private void FixedUpdate()
    {
        if (!gameOver)
            score = points + (int) playerTransform.position.z / 50;
    }
    
    public void GameOver()
    {
        gameOver = true;
        Time.timeScale = 0;
            
        if (score <= record)
        {
            scoreTitle.text = currentScoreTitle.Replace("####", score.ToString()).Replace("$$$$", record.ToString());
        }
        else
        {
            scoreTitle.text = currentScoreTitle.Replace("####", score.ToString()).Replace("Record:\n$$$$", "This is a new Record!");
            PlayerPrefs.SetInt("RECORD", score);
            PlayerPrefs.Save();
        }
        
        gameOverPanel.SetActive(true);
    }
    
    private IEnumerator Coroutine (string crtName)
    {
        switch (crtName)
        {
            case "SpeedIncrease":
                while (!gameOver){
                    yield return new WaitForSeconds(speedIncreaseSeconds);
                    Player.St.speed++;
                }    
                break;
        }
        yield break;
    }
}
