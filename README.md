# JingleRunner
v1.0 (2022)
======================================

- Game by AMARIAL (Abkhazia)
- Support by SkillBox (GameBox)

======================================

- Author: David Koyava
- Mentor: Andrey Aranovich

======================================

**Controls Swipe or Arrows:**
- ← →   Move
-  ↑   Jump

======================================

**Архив с игрой:**

https://yadi.sk/d/a7UYqE-kDT3t8A
